import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, DatePicker, TimePicker, message } from 'antd';
import 'antd/dist/antd.css';
import Axios from 'axios';

export class Predictor extends Component {
    
    state = {
        prediction: new prediction(), //object to be sent to the server
    }

    handleSubmit = values => {

        if(Number.isNaN(parseInt(values.plateNum.charAt(values.plateNum.length-1)))){
            
            message.error('Last digit of the plate must be an integer!')
        }
        else{
            this.setState(estado => {
                estado.prediction.plateNumber = values.plateNum.charAt(values.plateNum.length-1)
                estado.prediction.date = values.date._d.getDay()
                estado.prediction.hour = values.hour._d.getHours()
                estado.prediction.minute = values.hour._d.getMinutes()
                return estado
            })
            Axios.put(' https://y93raaay57.execute-api.us-east-1.amazonaws.com/dev', this.state.prediction)
                    .then(response => {
                        console.log(response)
                        if (response.data.roadCode === 1) {
                            message.success(response.data.message)
                        }
                        else{
                            message.warning(response.data.message)
                        }
                    })
                    .catch(error => console.log(error))
        }
    }



    render() {
        return (
            <Row justify='center'>
                <Col span={24}><h1>Predictor</h1></Col>
                <Form

                    name="predictor"
                    onFinish={this.handleSubmit}
                >

                    <Col span={24} >
                        <Form.Item
                            label="Plate Number"
                            name="plateNum"
                            rules={[{ required: true, message: 'Please input your plate number!' },
                                    {max: 7, message:'Max of 7 characters'},
                                    {min: 6, message:'Min of 6 characters'}]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            label="Date"
                            name="date"
                            rules={[{ required: true, message: 'Please input a date!' }]}
                        >
                            <DatePicker />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            label="Hour"
                            name="hour"
                            rules={[{ required: true, message: 'Please input an hour!' }]}
                        >
                            <TimePicker />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item >
                            <Button type="primary" htmlType="submit">
                                Predict
                        </Button>
                        </Form.Item>
                    </Col>
                </Form>
            </Row >
        )
    }
}

class prediction {
    plateNumber = null
    date = null
    hour = null
    minute = null
}

export default Predictor
