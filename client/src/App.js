import React from 'react';
import logo from './logo.svg';
import './App.css';
import Predictor from './Predictor/Predictor';

function App() {
  return (
    <div className="App">
      <Predictor></Predictor>
    </div>
  );
}

export default App;
