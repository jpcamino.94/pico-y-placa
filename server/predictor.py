import boto3
from boto3.dynamodb.conditions import Key, Attr
import json
with open('C:/Users/Sanchy/Documents/stackBuilders/pico-y-placa/server/awsKeys.json') as f:
  keys = json.load(f)

def lambda_handler(event, context):
    
    # message for road allow
    messageRoad = {
        'statusCode': 200,
        'message': "You can road!"
    } 

    # message for road deny
    messageDontRoad = {
        'statusCode': 200,
        'message': "You can't road!"
    }

    session = boto3.Session(aws_access_key_id = keys['access'], 
                        aws_secret_access_key = keys['secret'],
                        region_name = keys['region'])
    dynamodb = session.resource('dynamodb')
    table =  dynamodb.Table('diasPicoYPlaca')

    if event['date']  == 0 or event['date'] == 6:
        return messageRoad
    else:
        resp = table.query(
            KeyConditionExpression=Key('id').eq(event['date'])
        )
        
        plates = resp['Items'][0]['plates'].split(",") # arry of plates that can't road
               
        hourSelected = event['hour'] + (event['minute']/100) # format the selected hour and minute
        
        if event['plateNumber'] in plates and (7.0 <= hourSelected <= 9.3 or 16.0 <= hourSelected <= 19.3):
            return messageDontRoad
        else:
            return messageRoad

test={
    "date": 2,
    "hour": 5,
    "minute": 31,
    "plateNumber": '3'
}

print(lambda_handler(test,None))
